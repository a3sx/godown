package godown

import (
	"gitlab.com/a3sx/godown/pkg/client"
	"testing"
)

func Test(t *testing.T) {
	_, err := client.NewGDClient()
	if err != nil {
		t.Error(err)
	}
}
