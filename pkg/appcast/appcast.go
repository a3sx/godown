package appcast

import (
	"encoding/xml"
	"errors"
	"github.com/Masterminds/semver/v3"
	"github.com/shirou/gopsutil/v3/host"
	"gitlab.com/a3sx/godown/pkg/client"
	"gitlab.com/tymonx/go-formatter/formatter"
	"runtime"
)

type IAppcast interface {
	Get(client client.IClient, url string) error
	Content() (APCSparkle, error)
	proccess() error
	contains(item APCChannelItem) bool
	GetReleaseChannel(name string) (ReleaseChannel, error)
}

type Appcast struct {
	IAppcast
	url             string
	ContentXML      APCSparkle
	ReleaseChannels []ReleaseChannel
}

type GDAppcast struct {
	Appcast
}

type ReleaseChannel struct {
	Name   string
	Items  []APCChannelItem
	Latest APCChannelItem
}

func NewGDAppcast() (*GDAppcast, error) {
	apc := GDAppcast{}

	return &apc, nil
}

func (apc *GDAppcast) Get(client client.IClient, url string) error {
	apcFile, err := client.Get(url, nil)
	if err != nil {
		return err
	}

	err = xml.Unmarshal(apcFile, &apc.ContentXML)
	if err != nil {
		return err
	}

	err = apc.proccess()
	if err != nil {
		return err
	}

	return nil
}

func (apc *GDAppcast) Content() (APCSparkle, error) {
	return apc.ContentXML, nil
}

func (apc *GDAppcast) proccess() error {
	hostInfo, err := host.Info()
	if err != nil {
		return err
	}

	for _, item := range apc.ContentXML.Channel.Item {
		url, err := formatter.Format(item.Enclosure.URL, formatter.Named{
			"arch": runtime.GOARCH,
			"os":   hostInfo.OS,
		})
		if err != nil {
			return err
		}

		item.Enclosure.URL = url

		if !apc.contains(item) {
			rc := ReleaseChannel{
				Name:   item.Channel,
				Latest: item,
			}
			rc.Items = append(rc.Items, item)
			apc.ReleaseChannels = append(apc.ReleaseChannels, rc)
		}
	}

	return nil
}

func (apc *GDAppcast) contains(item APCChannelItem) bool {
	for i, v := range apc.ReleaseChannels {
		if v.Name == item.Channel {
			n, err := semver.NewVersion(item.Version)
			if err != nil {
				//return err
			}
			c, err := semver.NewVersion(apc.ReleaseChannels[i].Latest.Version)
			if err != nil {
				//return err
			}
			if n.GreaterThan(c) {
				apc.ReleaseChannels[i].Latest = item
			}
			apc.ReleaseChannels[i].Items = append(v.Items, item)
			return true
		}
	}

	return false
}

func (apc *GDAppcast) GetReleaseChannel(name string) (ReleaseChannel, error) {
	for _, v := range apc.ReleaseChannels {
		if v.Name == name {
			return v, nil
		}
	}
	return ReleaseChannel{}, errors.New("release channel doesnt exist")
}
