package appcast

import (
	"encoding/xml"
)

type APCSparkle struct {
	XMLName xml.Name `xml:"rss"`
	Sparkle string   `xml:"sparkle,attr"`
	Version string   `xml:"version,attr"`
	APCChannel
}

type APCChannel struct {
	Channel struct {
		Title string           `xml:"title"`
		Item  []APCChannelItem `xml:"item"`
	} `xml:"channel"`
}

type APCChannelItem struct {
	Title                string `xml:"title"`
	PubDate              string `xml:"pubDate"`
	Channel              string `xml:"channel"`
	Version              string `xml:"version"`
	ShortVersionString   string `xml:"shortVersionString"`
	MinimumSystemVersion string `xml:"minimumSystemVersion"`
	Enclosure            struct {
		URL    string `xml:"url,attr"`
		Length string `xml:"length,attr"`
		Type   string `xml:"type,attr"`
	} `xml:"enclosure"`
}
