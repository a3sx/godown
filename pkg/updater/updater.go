package updater

import (
	"archive/zip"
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
)

type IUpdater interface {
	Extract(body []byte, dir string) (string, error)
}

type GDUpdater struct {
	IUpdater
}

func NewGDUpdater() (*GDUpdater, error) {
	gdu := GDUpdater{}

	return &gdu, nil
}

func (up *GDUpdater) Extract(body []byte, dir string) (string, error) {
	name, err := unzip(body, dir)
	if err != nil {
		return "", err
	}
	return name, nil
}

func unzip(body []byte, dir string) (string, error) {
	zipReader, err := zip.NewReader(bytes.NewReader(body), int64(len(body)))
	if err != nil {
		return "", err
	}

	// Read all the files from zip archive
	for _, zipFile := range zipReader.File {
		// fmt.Println("Reading file:", zipFile.Name)
		unzippedFileBytes, err := readZipFile(zipFile)
		if err != nil {
			println(err)
			continue
		}

		fpath := filepath.Join(".", zipFile.Name)
		if zipFile.FileInfo().IsDir() {
			os.MkdirAll(dir+fpath, zipFile.Mode())
		} else {
			f, err := os.OpenFile(
				dir+fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, zipFile.Mode())
			if err != nil {
				return "", err
			}
			defer f.Close()

			_, err = f.Write(unzippedFileBytes)
			if err != nil {
				return "", err
			}

			if err != nil {
				return "", err
			}
		}
	}
	fl := zipReader.File[0]
	return fl.Name, nil
}

func readZipFile(zf *zip.File) ([]byte, error) {
	f, err := zf.Open()
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ioutil.ReadAll(f)
}
