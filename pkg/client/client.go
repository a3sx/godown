package client

import (
	"io/ioutil"
	"net/http"
)

type IClient interface {
	Get(url string, headers map[string]string) ([]byte, error)
}

type GDClient struct {
	IClient
	basicAuthToken string
	httpClient     *http.Client
}

func NewGDClient() (*GDClient, error) {
	httpClient := &http.Client{}

	gdc := GDClient{
		basicAuthToken: "",
		httpClient:     httpClient,
	}

	return &gdc, nil
}

func (clt *GDClient) Get(url string, headers map[string]string) ([]byte, error) {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	for key, value := range headers {
		request.Header.Add(key, value)
	}

	if clt.basicAuthToken != "" {
		request.Header.Add("Authorization", "Basic "+clt.basicAuthToken)
	}

	resp, err := clt.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return bodyBytes, nil
}

func (clt *GDClient) BasicAuth(token string) {
	clt.basicAuthToken = token
}
