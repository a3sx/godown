package godown

import (
	"net/rpc"
	"os"

	"github.com/Masterminds/semver/v3"
	"github.com/hashicorp/go-plugin"

	"gitlab.com/a3sx/godown/pkg/appcast"
	"gitlab.com/a3sx/godown/pkg/client"
	"gitlab.com/a3sx/godown/pkg/updater"
)

type GoDown struct {
	config GoDownConfig
}

type GoDownConfig struct {
	Client         client.IClient
	Updater        updater.IUpdater
	Appcast        appcast.IAppcast
	URL            string
	Dir            string
	Version        string
	Force          bool
	ReleaseChannel string
}

func New(config GoDownConfig) (*GoDown, error) {
	gd := GoDown{
		config: config,
	}

	return &gd, nil
}

func (gd *GoDown) Run() (string, error) {
	tempDir, err := os.MkdirTemp(os.TempDir(), "godown")
	if err != nil {
		return "", err
	}
	defer os.RemoveAll(tempDir)

	err = gd.config.Appcast.Get(gd.config.Client, gd.config.URL)
	if err != nil {
		return "", err
	}

	rc, err := gd.config.Appcast.GetReleaseChannel(gd.config.ReleaseChannel)
	if err != nil {
		return "", err
	}

	vc, err := semver.NewVersion(gd.config.Version)
	if err != nil {
		return "", err
	}
	vn, err := semver.NewVersion(rc.Latest.Version)
	if err != nil {
		return "", err
	}

	if vn.GreaterThan(vc) || gd.config.Force {
		body, err := gd.config.Client.Get(rc.Latest.Enclosure.URL, nil)
		if err != nil {
			return "", err
		}

		name, err := gd.config.Updater.Extract(body, tempDir+"/")
		if err != nil {
			return "", err
		}

		err = os.RemoveAll(gd.config.Dir + "/" + name)
		if err != nil {
			return "", err
		}

		err = os.Rename(tempDir+"/"+name, gd.config.Dir+"/"+name)
		if err != nil {
			return "", err
		}

	} else {
		return gd.config.Version, nil
	}

	return rc.Latest.Version, nil
}

type Update interface {
	Update(force bool, dir string) string
}

// Here is an implementation that talks over RPC
type UpdateRPC struct {
	client *rpc.Client
}

type UpdateArgs struct {
	Force bool
	Dir   string
}

func (g *UpdateRPC) Update(force bool, dir string) string {
	var resp string
	err := g.client.Call("Plugin.Update", UpdateArgs{
		Force: force,
		Dir:   dir,
	}, &resp)
	if err != nil {
		// You usually want your interfaces to return errors. If they don't,
		// there isn't much other choice here.
		panic(err)
	}
	return resp
}

type UpdateRPCServer struct {
	Impl Update
}

func (s *UpdateRPCServer) Update(args UpdateArgs, resp *string) error {
	*resp = s.Impl.Update(args.Force, args.Dir)
	return nil
}

type UpdatePlugin struct {
	// Impl Injection
	Impl Update
}

func (p *UpdatePlugin) Server(*plugin.MuxBroker) (interface{}, error) {
	return &UpdateRPCServer{Impl: p.Impl}, nil
}

func (UpdatePlugin) Client(b *plugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &UpdateRPC{client: c}, nil
}
